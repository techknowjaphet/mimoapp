package com.mimo.code.ux.views

import android.content.Context
import android.graphics.Color
import android.text.*
import android.text.style.ForegroundColorSpan
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.mimo.code.R
import com.mimo.code.db.entities.LessonWithContent
import java.text.SimpleDateFormat
import java.util.*

interface MimoLessonViewCallBack {
    fun onRendered(hasInput: Boolean, solution: String?)
    fun onInputEntered(s: CharSequence?)
}

class MimoLessonView : LinearLayout {
    private lateinit var mimoLessonViewCallBack: MimoLessonViewCallBack

    constructor(context: Context) : super(context) {
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
    }

    init {
        orientation = VERTICAL
    }

    fun setCallBack(callBack: MimoLessonViewCallBack) {
        mimoLessonViewCallBack = callBack
    }

    fun addViews(lesson: LessonWithContent) {
        removeAllViews()
        val container = LinearLayout(context)
        container.layoutParams =
            LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        container.orientation = HORIZONTAL
        val scale = resources.displayMetrics.density
        val containerPadding = (20 * scale + 0.5f)

        container.setPadding(0, containerPadding.toInt(), 0, containerPadding.toInt())
        container.setBackgroundColor(Color.parseColor("#FF018786"))

        var textView = TextView(context)
        textView.layoutParams =
            LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        val timePadding = (20 * scale + 0.5f)
        textView.setPadding(
            timePadding.toInt(),
            timePadding.toInt(),
            timePadding.toInt(),
            timePadding.toInt()
        )

        val timeString = StringBuilder()
        timeString.append("Started ${getDate(lesson.lesson.startTimeStamp)}")
            .append(System.getProperty("line.separator"))
        if (lesson.lesson.completedTimeStamp == -1L) {
            timeString.append("Not yet complete")
        } else {
            timeString.append("Completed ${getDate(lesson.lesson.completedTimeStamp)}")
        }
        textView.text = timeString
        addView(textView)

        var length = 0
        var inputViewIndex = -1
        var solution: String? = null
        lesson.content.forEachIndexed { index, content ->
            var spannable = SpannableString(content.text)
            var textView = TextView(context)
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20.0f);
            if (lesson.lesson.startIndex != null && lesson.lesson.endIndex != null
                && length == lesson.lesson.startIndex
            ) {
                inputViewIndex = index
                solution = content.text
                textView = EditText(context)
                textView.setTextColor(Color.parseColor(content.color))
                textView.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                    }

                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        mimoLessonViewCallBack.let {
                            it.onInputEntered(s)
                        }
                    }
                })
                textView.minEms = content.text.length
                textView.setBackgroundResource(R.drawable.input_background);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12.0f);
            } else {
                spannable.setSpan(
                    ForegroundColorSpan(Color.parseColor(content.color)),
                    0,
                    content.text.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                textView.text = spannable
            }
            length += content.text.length
            textView.layoutParams =
                LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
            textView.setPadding(
                timePadding.toInt(),
                timePadding.toInt(),
                timePadding.toInt(),
                timePadding.toInt()
            )
            container.addView(textView)
        }
        addView(container)
        if (inputViewIndex != -1) {
            val inputView = container.getChildAt(inputViewIndex) as EditText
            inputView.requestFocusFromTouch()
            inputView.requestFocus()
            val imm =
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(inputView, InputMethodManager.SHOW_IMPLICIT)
        }
        mimoLessonViewCallBack.onRendered(inputViewIndex != -1, solution)
    }

    fun getDate(date: Long): String {
        val date = Date(date)
        val df2 = SimpleDateFormat("dd-MMM-yyyy HH:mm:ss")
        return df2.format(date)
    }
}