package com.mimo.code.ux.activities

import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import com.google.android.material.snackbar.Snackbar
import com.mimo.code.R
import com.mimo.code.backend.db.MimoDatabase.Companion.getDatabase
import com.mimo.code.databinding.ActivityMainBinding
import com.mimo.code.network.getNetworkService
import com.mimo.code.utils.MimoRepository
import com.mimo.code.ux.vm.MainViewModel


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.lifecycleOwner = this  // use Fragment.viewLifecycleOwner for fragments

        val database = getDatabase(this)
        val repository =
            MimoRepository(getNetworkService(), database.lessonDao, database.contentDao)
        val viewModel = ViewModelProviders
            .of(this, MainViewModel.FACTORY(repository))
            .get(MainViewModel::class.java)

        binding.viewmodel = viewModel

        viewModel.lessons.observe(owner = this) {
            for (lesson in it) {
                if (lesson.lesson.completedTimeStamp == -1L) {
                    viewModel.setCurrentLesson(lesson)
                    break
                }
            }
        }


        viewModel.show_keyboard.observe(this) {
            if (!it) {
                val imm: InputMethodManager =
                    getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(
                    getWindow().getDecorView().getRootView().getWindowToken(), 0
                )
            }
        }

        viewModel.snackbar.observe(this) { text ->
            text?.let {
                Snackbar.make(binding.root, text, Snackbar.LENGTH_SHORT).show()
                viewModel.onSnackbarShown()
            }
        }
        viewModel.refreshLessons()
    }
}