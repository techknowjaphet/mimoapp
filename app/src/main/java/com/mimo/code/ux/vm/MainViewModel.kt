package com.mimo.code.ux.vm

import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.*
import com.example.android.kotlincoroutines.util.singleArgViewModelFactory
import com.mimo.code.R
import com.mimo.code.db.entities.Lesson
import com.mimo.code.db.entities.LessonWithContent
import com.mimo.code.utils.MimoError
import com.mimo.code.utils.MimoRepository
import com.mimo.code.ux.views.MimoLessonViewCallBack
import kotlinx.coroutines.launch

class MainViewModel(private val repository: MimoRepository) : ViewModel(), MimoLessonViewCallBack {
    companion object {
        val FACTORY = singleArgViewModelFactory(::MainViewModel)
    }

    private val _show_keyboard = MutableLiveData(false)
    val show_keyboard: MutableLiveData<Boolean> = _show_keyboard

    private val _solution_text = MutableLiveData("")

    private val _button_text = MutableLiveData("Next")
    val button_text: LiveData<String> = _button_text

    private val _button_enabled = MutableLiveData(false)
    val button_enabled: LiveData<Boolean> = _button_enabled

    val lessons = repository.lessons.asLiveData()
    private val _currentLesson = MutableLiveData<LessonWithContent?>()
    val currentLesson: LiveData<LessonWithContent?> get() = _currentLesson


    fun setCurrentLesson(lesson: LessonWithContent) {
        viewModelScope.launch {
            if (lesson.lesson.startTimeStamp == -1L) {
                lesson.lesson.startTimeStamp = System.currentTimeMillis()
                repository.updateLesson(
                    Lesson(
                        lesson.lesson.lessonId,
                        lesson.lesson.startIndex,
                        lesson.lesson.endIndex,
                        lesson.lesson.startTimeStamp,
                        -1
                    )
                )
            }
            _currentLesson.value = lesson
        }
    }

    val lessonViewCallBack = this

    private val _snackBar = MutableLiveData<String?>()
    val snackbar: LiveData<String?>
        get() = _snackBar

    private val _done = MutableLiveData<Boolean>(false)
    val done: LiveData<Boolean>
        get() = _done

    private val _spinner = MutableLiveData<Boolean>(false)
    val spinner: LiveData<Boolean>
        get() = _spinner


    fun onSnackbarShown() {
        _snackBar.value = null
    }

    fun refreshLessons() = launchDataLoad {
        repository.refreshLessons()
    }

    private fun launchDataLoad(block: suspend () -> Unit): Unit {
        viewModelScope.launch {
            try {
                _spinner.value = true
                block()
            } catch (error: MimoError) {
                _snackBar.value = error.message
            } finally {
                _spinner.value = false
            }
        }
    }

    fun onBtnClicked() {
        _done.value?.let {
            if (it) {
                _done.value = false
                _button_text.value = "Next"
                refreshLessons()
                return
            }
        }
        lessons.value?.let {
            if (it.indexOf(_currentLesson.value) != it.size - 1) {
                viewModelScope.launch {
                    _currentLesson.value?.let { lesson ->
                        if (lesson.lesson.completedTimeStamp == -1L) {
                            lesson.lesson.completedTimeStamp = System.currentTimeMillis()
                            repository.updateLesson(
                                Lesson(
                                    lesson.lesson.lessonId,
                                    lesson.lesson.startIndex,
                                    lesson.lesson.endIndex,
                                    lesson.lesson.startTimeStamp,
                                    lesson.lesson.completedTimeStamp
                                )
                            )
                        }
                    }
                }
            } else {
                _done.value = true
                _button_text.value = "Restart"
            }
        }

    }

    override fun onRendered(hasInput: Boolean, solution: String?) {
        show_keyboard.value = hasInput
        _button_enabled.value = !hasInput
        _solution_text.value = solution
    }

    override fun onInputEntered(s: CharSequence?) {
        if (!TextUtils.isEmpty(_solution_text.value)) {
            _button_enabled.value =
                !TextUtils.isEmpty(s) && _solution_text.value.equals(s.toString())
        }
    }
}