package com.mimo.code.network.models

data class ContentModel(val lessonId: Int, val text: String, val color: String)
