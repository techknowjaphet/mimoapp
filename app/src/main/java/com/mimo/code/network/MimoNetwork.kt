package com.mimo.code.network

import com.google.gson.GsonBuilder
import com.mimo.code.network.helpers.LessonsDeserializer
import com.mimo.code.network.models.LessonsResponse
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

private val service: MimoNetwork by lazy {

    val lessonsDeserializer =
        GsonBuilder().registerTypeAdapter(LessonsResponse::class.java, LessonsDeserializer())
            .create()
    val retrofit = Retrofit.Builder()
        .baseUrl("https://mimochallenge.azurewebsites.net/api/")
        .client(
            OkHttpClient.Builder()
                .build()
        )
        .addConverterFactory(GsonConverterFactory.create(lessonsDeserializer))
        .build()

    retrofit.create(MimoNetwork::class.java)
}

fun getNetworkService() = service


interface MimoNetwork {
    @GET("lessons")
    suspend fun fetchLessons(): LessonsResponse
}