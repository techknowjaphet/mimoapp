package com.mimo.code.network.helpers

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.mimo.code.network.models.ContentModel
import com.mimo.code.network.models.LessonModel
import com.mimo.code.network.models.LessonsResponse
import java.lang.reflect.Type

class LessonsDeserializer : JsonDeserializer<LessonsResponse> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): LessonsResponse {
        val jsonObject = json?.asJsonObject
        val lessonsJsonArray = jsonObject?.getAsJsonArray("lessons")
        val lessonsArray: MutableList<LessonModel> = arrayListOf()
        lessonsJsonArray?.forEach { lessonJsonObject ->
            val contentsJsonArray = lessonJsonObject?.asJsonObject?.getAsJsonArray("content")
            val lessonId = lessonJsonObject?.asJsonObject?.get("id")?.asInt!!
            val contentArray: MutableList<ContentModel> = arrayListOf()
            contentsJsonArray?.forEach { contentJson ->
                contentArray.add(
                    ContentModel(
                        lessonId,
                        contentJson?.asJsonObject?.get("text")?.asString!!,
                        contentJson?.asJsonObject?.get("color")?.asString!!
                    )
                )
            }
            val startIndex =
                lessonJsonObject?.asJsonObject?.get("input")?.asJsonObject?.get("startIndex")?.asInt
            val endIndex =
                lessonJsonObject?.asJsonObject?.get("input")?.asJsonObject?.get("endIndex")?.asInt
            lessonsArray.add(
                LessonModel(
                    lessonId,
                    startIndex,
                    endIndex,
                    contentArray
                )
            )
        }
        return LessonsResponse(lessonsArray)


    }
}