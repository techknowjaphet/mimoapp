package com.mimo.code.network.models


data class LessonsResponse(
    val lessons: MutableList<LessonModel>
)
