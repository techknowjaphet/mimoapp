package com.mimo.code.network.models

data class LessonModel(
    val id: Int,
    val startIndex: Int?,
    val endIndex: Int?,
    val content: List<ContentModel>
)