package com.example.android.kotlincoroutines.util

import android.view.View
import android.widget.Button
import androidx.databinding.BindingAdapter
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mimo.code.R
import com.mimo.code.db.entities.LessonWithContent
import com.mimo.code.ux.views.MimoLessonView
import com.mimo.code.ux.views.MimoLessonViewCallBack


fun <T : ViewModel, A> singleArgViewModelFactory(constructor: (A) -> T):
            (A) -> ViewModelProvider.NewInstanceFactory {
    return { arg: A ->
        object : ViewModelProvider.NewInstanceFactory() {
            @Suppress("UNCHECKED_CAST")
            override fun <V : ViewModel> create(modelClass: Class<V>): V {
                return constructor(arg) as V
            }
        }
    }
}

@BindingAdapter("app:lessonContent")
fun lessonContent(view: MimoLessonView, lesson: LessonWithContent?) {
    lesson?.let { view.addViews(it) }
}

@BindingAdapter("app:lessonViewCallBack")
fun lessonViewCallBack(view: MimoLessonView, callBack: MimoLessonViewCallBack?) {
    callBack?.let { view.setCallBack(it) }
}

@BindingAdapter("app:mimoVisibility")
fun mimoVisibility(view: View, visible: Boolean?) {
    visible?.let {
        view.visibility = if (it) View.VISIBLE else View.GONE
    }
}

@BindingAdapter("app:stateBackground")
fun stateBackground(view: Button, enabled: Boolean?) {
    enabled?.let {
        view.setBackgroundColor(
            if (it) view.resources.getColor(R.color.purple_700) else view.resources.getColor(
                R.color.purple_200
            )
        )
    }
}

