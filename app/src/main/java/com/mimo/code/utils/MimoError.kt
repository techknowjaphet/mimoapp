package com.mimo.code.utils

class MimoError(message: String, cause: Throwable) : Throwable(message, cause)
