package com.mimo.code.utils

import com.mimo.code.db.daos.ContentDao
import com.mimo.code.db.daos.LessonDao
import com.mimo.code.db.entities.Content
import com.mimo.code.db.entities.Lesson
import com.mimo.code.db.entities.LessonWithContent
import com.mimo.code.network.MimoNetwork
import kotlinx.coroutines.flow.Flow

class MimoRepository(
    val network: MimoNetwork,
    val lessonDao: LessonDao,
    val contentDao: ContentDao
) {
    val lessons: Flow<List<LessonWithContent>> = lessonDao.lessonWithContent()

    suspend fun updateLesson(lesson: Lesson) {
        lessonDao.update(lesson)
    }

    suspend fun refreshLessons() {
        try {
            val result = network.fetchLessons()
            val lessons: MutableList<Lesson> = mutableListOf()
            val contents: MutableList<Content> = mutableListOf()
            result.lessons.forEach { lesson ->
                lessons.add(Lesson(lesson.id, lesson.startIndex, lesson.endIndex, -1, -1))
                lesson.content.forEach { content ->
                    contents.add(Content(0, lesson.id, content.color, content.text))
                }
            }
            lessonDao.deleteAllLessons()
            contentDao.deleteAllContent()
            lessonDao.insertLessons(lessons)
            contentDao.insertContents(contents)
        } catch (error: Throwable) {
            throw MimoError("Unable to fetch lessons", error)
        }
    }
}