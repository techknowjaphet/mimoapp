package com.mimo.code.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Lesson(
    @PrimaryKey
    val lessonId: Int,
    val startIndex: Int?,
    val endIndex: Int?,
    var startTimeStamp: Long,
    var completedTimeStamp: Long
)