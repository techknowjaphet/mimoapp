package com.mimo.code.backend.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mimo.code.db.daos.ContentDao
import com.mimo.code.db.daos.LessonDao
import com.mimo.code.db.entities.Content
import com.mimo.code.db.entities.Lesson

@Database(entities = [Lesson::class, Content::class], version = 1)
abstract class MimoDatabase : RoomDatabase() {
    abstract val lessonDao: LessonDao
    abstract val contentDao: ContentDao

    companion object {
        @Volatile
        private var INSTANCE: MimoDatabase? = null

        fun getDatabase(context: Context): MimoDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MimoDatabase::class.java,
                    "mimo_db"
                )
                    //no need for migrations here
                    .fallbackToDestructiveMigration()
                    .build()

                INSTANCE = instance
                // return instance
                instance
            }
        }


    }
}