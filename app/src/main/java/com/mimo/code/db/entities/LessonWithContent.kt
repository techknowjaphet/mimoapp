package com.mimo.code.db.entities

import androidx.room.Embedded
import androidx.room.Relation

data class LessonWithContent(
    @Embedded
    val lesson: Lesson,
    @Relation(parentColumn = "lessonId", entityColumn = "parentLessonId")
    val content: List<Content>
)
