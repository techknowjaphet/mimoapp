package com.mimo.code.db.daos

import androidx.room.*
import com.mimo.code.db.entities.Content
import com.mimo.code.db.entities.Lesson

@Dao
interface ContentDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertContents(content: List<Content>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertContent(content: Content): Long

    @Query("DELETE FROM Content")
    suspend fun deleteAllContent()
}