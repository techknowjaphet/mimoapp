package com.mimo.code.db.daos

import androidx.room.*
import com.mimo.code.db.entities.LessonWithContent
import com.mimo.code.db.entities.Lesson
import kotlinx.coroutines.flow.Flow

@Dao
interface LessonDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertLesson(lesson: Lesson): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertLessons(lesson: MutableList<Lesson>)

    @Update
    suspend fun update(lesson: Lesson)

    @Query("DELETE FROM Lesson")
    suspend fun deleteAllLessons()

    @Query("SELECT * FROM Lesson")
    fun lessons(): Flow<List<Lesson>>

    @Transaction
    @Query("SELECT * FROM Lesson")
    fun lessonWithContent(): Flow<List<LessonWithContent>>
}