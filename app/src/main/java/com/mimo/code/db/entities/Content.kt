package com.mimo.code.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Content(
    @PrimaryKey(autoGenerate = true)
    val contentId: Int,
    val parentLessonId: Int,
    val color: String,
    val text: String
)
