package com.mimo.code

import com.mimo.code.db.daos.ContentDao
import com.mimo.code.db.entities.Content

class ContentDaoFake(content: Content) : ContentDao {
    override suspend fun insertContents(content: List<Content>) {

    }

    override suspend fun insertContent(content: Content): Long {
        return 1L
    }

    override suspend fun deleteAllContent() {

    }
}